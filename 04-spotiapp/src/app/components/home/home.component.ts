import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  nuevasCanciones: any[] = [];
  loading: boolean;
  error:boolean = false;
  descError: any = {
    message: '',
    status: 0
  };
  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.spotify.getNewReleases().subscribe(resp => {
      this.nuevasCanciones = resp
    }, (respError =>{
      this.descError = {
        message: respError.error.error.message,
        status: respError.error.error.status
      }
      this.error = true;
    }));
    this.loading = false;
  }

  ngOnInit(): void {}

}
