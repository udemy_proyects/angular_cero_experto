import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }
  private endpoint:string = 'https://api.spotify.com/v1/';

  private token: string = 'BQAogK8A5VAVBv1vXvifN2Q8K7Ry9S_fyBxe53oCV36mBSKaBWVUYLBV5fKw9OJVDTk1iogT3frjbpJTDCQ';
  private tipo_token = 'Bearer ' + this.token;
  private listNewUrl: string = 'browse/new-releases?limit=50';
  private artistUrl: string = 'search?q=';
  private artistsInfo:string = 'artists/';

  private artistsTopTrack:string = '/top-tracks?country=us';
  getQuery(query:string){
    let url = this.endpoint + query;
    const headers = new HttpHeaders({
      'Authorization': this.tipo_token
    });    
    return this.http.get(url, {headers});
  }

  getNewReleases() {
    return this.getQuery(this.listNewUrl).pipe(map(data => data['albums'].items));
  }

  getArtists(termino: string) {
    return this.getQuery(this.artistUrl + termino + '&type=artist').pipe(map(resp => resp['artists'].items));
  }

  getArtist(id:string){
    return this.getQuery(this.artistsInfo + id ).pipe(map(resp => resp));
  }

  
  getArtistsTopTracks(id:string){
    return this.getQuery(this.artistsInfo + id + this.artistsTopTrack).pipe(map(resp => resp['tracks']));
  }
}
