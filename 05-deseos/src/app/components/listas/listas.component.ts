import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList } from '@ionic/angular';
import { Lista } from 'src/app/models/Lista.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  @Input() terminados = true;
  @ViewChild(IonList) lista:IonList;
  constructor(private deseosService: DeseosService, private router: Router, private alertCtrl: AlertController) {
  }

  ngOnInit() { }

  listaSeleccionada(lista: Lista) {
    if (this.terminados) {
      this.router.navigateByUrl(`/terminados/agregar/${lista.id}`);
    } else {
      this.router.navigateByUrl(`/pendientes/agregar/${lista.id}`);
    }
  }

  async editList(lista: Lista) {
    const alert = await this.alertCtrl.create({
      header: lista.titulo,
      inputs: [{
        name: 'titulo',
        type: 'text',
        placeholder: 'Nombre de la lista'
      }
      ],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      }, {
        text: 'Actualizar',
        handler: (data) => {
          if (data.titulo.length === 0) {
            return;
          }
          lista.titulo = data.titulo;
          this.deseosService.guardarStorage();
          this.lista.closeSlidingItems();
        }
      }]
    });
    alert.present();
  }


}
