import { Pipe, PipeTransform } from '@angular/core';
import { Lista } from '../models/Lista.model';

@Pipe({
  name: 'filtroTermiandos',
  pure:false
})
export class FiltroTermiandosPipe implements PipeTransform {

  transform(listas:Lista[], completada: boolean= false): Lista[] {
    return listas.filter(lista => lista.terminado === completada);
  }

}
