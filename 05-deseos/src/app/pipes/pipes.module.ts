import { NgModule } from '@angular/core';
import { FiltroTermiandosPipe } from './filtro-termiandos.pipe';



@NgModule({
  declarations: [FiltroTermiandosPipe],
  exports:[FiltroTermiandosPipe]
})
export class PipesModule { }
