import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2'

import { AuthService } from '../../services/auth.service';

import { UsuarioModel } from '../../models/usuario.model';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();;
  recordar:boolean = false;

  constructor(private auth: AuthService, private router:Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {

    if (form.invalid) return;

    Swal.fire({
      allowOutsideClick:false,
      type: 'info',
      text: 'Espere por favor...'
    });

    Swal.showLoading();
    this.auth.newUser(this.usuario).subscribe(response =>{
        console.log(response);
        Swal.close();
        if(this.recordar) localStorage.setItem('email', this.usuario.email);
        this.router.navigateByUrl('/home');
    }, (err)=>{
        Swal.fire({
          type: 'error',
          title: 'Error al registrar',
          text: err.error.error.message
        });
    })

  }

}
