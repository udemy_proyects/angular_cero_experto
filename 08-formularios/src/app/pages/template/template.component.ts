import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


import { PaisesService } from '../../services/paises.service';
@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

paises:any[] = [];
  usuario ={
    nombre: 'Erik',
    apellido: 'Guzman',
    email: 'erik.guzman@gluo.mx',
    pais: '',
    genero: 'M'
  }
  constructor(private paisesService:PaisesService) { }

  ngOnInit(): void {
    this.paisesService.getPaises().subscribe(paises =>{
      this.paises = paises;
      this.paises.unshift({
        nombre: 'Seleccione un pais',
        codigo: ''
      })
    })
  }

  guardar(forma: NgForm){
    console.log(forma);
    console.log(forma.value);
    if(forma.invalid){
      Object.values(forma.controls).forEach(control =>{
        control.markAsTouched();
      });
      return;
    }

  }
}
